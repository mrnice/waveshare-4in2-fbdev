// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * DRM driver for Waveshare branded e-ink panels
 *
 * Copyright 2022 Bernhard Guillon
 *
 * Based on repaper.c
 * Copyright 2013-2017 Pervasive Displays, Inc.
 * Copyright 2017 Noralf Trønnes
 *
 * Loosly based on:
 * https://github.com/nxthongbk/MangoH-Waveshare
 * Copyright (C) 2018 Thong Nguyen
 *
 * https://github.com/dpfrey/mangOH_Waveshare_Eink
 * Copyright (C) 2018 David Frey
 *
 * The quick look up table code is taken from
 * https://benkrasnow.blogspot.com/2017/10/fast-partial-refresh-on-42-e-paper.html
 * and is Public Domain
 *
 * The driver supports:
 * Waveshare eink device e4200
 *
 * Copyright (C) Waveshare team (MIT)
 * The controller code was taken from the userspace driver:
 * https://github.com/waveshare/e-Paper
 *
 * | File      	:   EPD_4in2.c
 * | Author      :   Waveshare team
 * | Function    :   4.2inch e-paper
 * | Info        :
 *----------------
 * |	This version:   V3.1
 * | Date        :   2019-11-14
 * | Info        :
 * -----------------------------------------------------------------------------
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include <linux/delay.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/mod_devicetable.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/property.h>
#include <linux/spi/spi.h>

#include <drm/drm_atomic_helper.h>
#include <drm/drm_connector.h>
#include <drm/drm_damage_helper.h>
#include <drm/drm_drv.h>
#include <drm/drm_fb_cma_helper.h>
#include <drm/drm_fb_helper.h>
#include <drm/drm_format_helper.h>
#include <drm/drm_gem_atomic_helper.h>
#include <drm/drm_gem_cma_helper.h>
#include <drm/drm_gem_framebuffer_helper.h>
#include <drm/drm_managed.h>
#include <drm/drm_modes.h>
#include <drm/drm_probe_helper.h>
#include <drm/drm_rect.h>
#include <drm/drm_simple_kms_helper.h>

#define UC8176_PANEL_SETTING 0x00
#define UC8176_POWER_SETTING 0x01
#define UC8176_POWER_ON 0x04
#define UC8176_BOOSTER_SOFT_START 0x06
#define UC8176_DISPLAY_START_TRANSMISSION_1 0x10
#define UC8176_DATA_STOP 0x11
#define UC8176_DISPLAY_REFRESH 0x12
#define UC8176_DISPLAY_START_TRANSMISSION_2 0x13
#define VCOM_LUT 0x20
#define W2W_LUT 0x21
#define B2W_LUT 0x22
#define W2B_LUT 0x23
#define B2B_LUT 0x24
#define PLL_CONTROL 0x30
#define UC8176_VOM_AND_DATA_INTERVAL_SETTING 0x50
#define UC8176_RESOLUTION_SETTING 0x61
#define UC8176_VCM_DC_SETTING 0x82
#define UC8176_PARTIAL_WINDOW 0x90
#define UC8176_PARTIAL_IN 0x91
#define UC8176_PARTIAL_OUT 0x92

// FIMXE: Use return codes or make the functions void
// FIMXE: Use /* */ sequences instead of //
// FIXME: Flashy border, it might be possible to do some tricks like repaper
// FIXME: does?
// FIXME: Sort functions to waveshare uc uc8176 and so on.
// FIXME: Linux kernel now supports C11, therefore use that and move
//        declarations to the used area
// FIXME: Also port the rest to C11
// FIMXE: make sure we don't include stuff we do not use!

/* This might look a bit overcomplicated, but I own a 2.8 inch device which I
 * want to support as well
 */
enum waveshare_model {
  /* 0 is reserved to avoid clashing with NULL */
  E4200 = 1,
};

struct waveshare_epd {
  struct drm_device drm;
  struct drm_simple_display_pipe pipe;
  const struct drm_display_mode *mode;
  struct drm_connector connector;
  struct spi_device *spi;

  struct gpio_desc *border;
  struct gpio_desc *dc;
  struct gpio_desc *reset;
  struct gpio_desc *busy;

  unsigned int height;
  unsigned int width;
  unsigned int bytes_per_scan;
  unsigned int stage_time;
  unsigned int factored_stage_time;

  u8 *line_buffer;
  void *current_frame;
};

/*
 * Original look up tables taken from waveshare EPD_4in2.c
 * These tables are slow but save to use without any burnins.
 */
static const u8 lut_vcom0[] = {
    0x00, 0x17, 0x00, 0x00, 0x00, 0x02, 0x00, 0x17, 0x17, 0x00, 0x00,
    0x02, 0x00, 0x0A, 0x01, 0x00, 0x00, 0x01, 0x00, 0x0E, 0x0E, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
static const u8 lut_ww[] = {
    0x40, 0x17, 0x00, 0x00, 0x00, 0x02, 0x90, 0x17, 0x17, 0x00, 0x00,
    0x02, 0x40, 0x0A, 0x01, 0x00, 0x00, 0x01, 0xA0, 0x0E, 0x0E, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
static const u8 lut_bw[] = {
    0x40, 0x17, 0x00, 0x00, 0x00, 0x02, 0x90, 0x17, 0x17, 0x00, 0x00,
    0x02, 0x40, 0x0A, 0x01, 0x00, 0x00, 0x01, 0xA0, 0x0E, 0x0E, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
static const u8 lut_wb[] = {
    0x80, 0x17, 0x00, 0x00, 0x00, 0x02, 0x90, 0x17, 0x17, 0x00, 0x00,
    0x02, 0x80, 0x0A, 0x01, 0x00, 0x00, 0x01, 0x50, 0x0E, 0x0E, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
static const u8 lut_bb[] = {
    0x80, 0x17, 0x00, 0x00, 0x00, 0x02, 0x90, 0x17, 0x17, 0x00, 0x00,
    0x02, 0x80, 0x0A, 0x01, 0x00, 0x00, 0x01, 0x50, 0x0E, 0x0E, 0x00,
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

/*
 * Quick look up tables taken from
 * https://benkrasnow.blogspot.com/2017/10/fast-partial-refresh-on-42-e-paper.html
 *
 * These tables are remarkably fast and offer framerates about 3Hz.
 * They work for me(tm) without burnins but they may reduce the lifespan of your
 * device.
 */
const u8 lut_vcom0_quick[] = {
    0x00, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

const u8 lut_ww_quick[] = {
    0xA0, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

const u8 lut_bw_quick[] = {
    0xA0, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

const u8 lut_bb_quick[] = {
    0x50, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

const u8 lut_wb_quick[] = {
    0x50, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static int uc8176_write_buffer(struct waveshare_epd *epd, const u8 *txbuf,
                                 size_t size) {
  gpiod_set_value_cansleep(epd->dc, 1);
  return spi_write(epd->spi, txbuf, size);
}

static int uc8176_write_data(struct waveshare_epd *epd, u8 data) {
  return uc8176_write_buffer(epd, &data, 1);
}

static int uc8176_write_cmd(struct waveshare_epd *epd, u8 cmd) {
  gpiod_set_value_cansleep(epd->dc, 0);
  return spi_write(epd->spi, &cmd, 1);
}

static void uc8176_write_lut(struct waveshare_epd *epd, u8 lut_register,
                             const u8 *lut, size_t lut_size) {
  uc8176_write_cmd(epd, lut_register);
  uc8176_write_buffer(epd, lut, lut_size);
}

static void set_lut(struct waveshare_epd *epd) {
  uc8176_write_lut(epd, 0x20, lut_vcom0, ARRAY_SIZE(lut_vcom0));
  uc8176_write_lut(epd, 0x21, lut_ww, ARRAY_SIZE(lut_ww));
  uc8176_write_lut(epd, 0x22, lut_bw, ARRAY_SIZE(lut_bw));
  uc8176_write_lut(epd, 0x23, lut_wb, ARRAY_SIZE(lut_wb));
  uc8176_write_lut(epd, 0x24, lut_bb, ARRAY_SIZE(lut_bb));
}

static void set_lut_quick(struct waveshare_epd *epd) {
  uc8176_write_lut(epd, 0x20, lut_vcom0_quick, ARRAY_SIZE(lut_vcom0_quick));
  uc8176_write_lut(epd, 0x21, lut_ww_quick, ARRAY_SIZE(lut_ww_quick));
  uc8176_write_lut(epd, 0x22, lut_bw_quick, ARRAY_SIZE(lut_bw_quick));
  uc8176_write_lut(epd, 0x23, lut_wb_quick, ARRAY_SIZE(lut_wb_quick));
  uc8176_write_lut(epd, 0x24, lut_bb_quick, ARRAY_SIZE(lut_bb_quick));
}

static void display_frame_quick(struct waveshare_epd *epd) {
  set_lut_quick(epd);
  uc8176_write_cmd(epd, UC8176_DISPLAY_REFRESH);
}

static inline struct waveshare_epd *drm_to_epd(struct drm_device *drm) {
  return container_of(drm, struct waveshare_epd, drm);
}

static void waveshare_gray8_to_mono(u8 *buf, u32 width, u32 height) {
  u8 *gray8 = buf, *mono = buf;
  int y, xb, i;

  for (y = 0; y < height; y++)
    for (xb = 0; xb < width / 8; xb++) {
      u8 byte = 0x00;

      for (i = 0; i < 8; i++) {
        int x = xb * 8 + i;

        byte <<= 1;
        if (gray8[y * width + x] >> 7)
          byte |= BIT(0);
      }
      *mono++ = byte;
    }
}

static void uc8176_display_transmission_1(struct waveshare_epd *epd,
                                          u8 *image_buffer) {
  uc8176_write_cmd(epd, UC8176_DISPLAY_START_TRANSMISSION_1);
  uc8176_write_buffer(epd, image_buffer, epd->height * epd->width / 8);
}

static void uc8176_display_transmission_2(struct waveshare_epd *epd,
                                          u8 *image_buffer) {
  uc8176_write_cmd(epd, UC8176_DISPLAY_START_TRANSMISSION_2);
  uc8176_write_buffer(epd, image_buffer, epd->height * epd->width / 8);
}

static bool wait_for_display(struct waveshare_epd *epd) {
  int i;
  for (i = 100; i > 0; i--) {
    if (!gpiod_get_value_cansleep(epd->busy))
      break;
    usleep_range(10, 100);
  }
  if (!i)
    return false;
  return true;
}

static void wait_for_redraw(struct waveshare_epd *epd, u8 *buf) {
  u64 wait_time;
  wait_time = epd->factored_stage_time * 1000;
  uc8176_display_transmission_1(epd, buf);
  display_frame_quick(epd);
  usleep_range(wait_time - 2000, wait_time);
}

static int waveshare_fb_dirty(struct drm_framebuffer *fb) {
  struct drm_gem_cma_object *cma_obj = drm_fb_cma_get_gem_obj(fb, 0);
  struct waveshare_epd *epd = drm_to_epd(fb->dev);
  struct drm_rect clip;
  int idx, ret = 0;
  u8 *buf = NULL;

  if (!drm_dev_enter(fb->dev, &idx))
    return -ENODEV;

  /* Waveshare could do partial updates but it is not faster then redawing */
  clip.x1 = 0;
  clip.x2 = fb->width;
  clip.y1 = 0;
  clip.y2 = fb->height;

  DRM_DEBUG("Flushing [FB:%d] st=%ums\n", fb->base.id,
            epd->factored_stage_time);

  buf = kmalloc_array(fb->width, fb->height, GFP_KERNEL);
  if (!buf) {
    ret = -ENOMEM;
    goto out_exit;
  }

  ret = drm_gem_fb_begin_cpu_access(fb, DMA_FROM_DEVICE);
  if (ret)
    goto out_free;

  drm_fb_xrgb8888_to_gray8(buf, cma_obj->vaddr, fb, &clip);

  drm_gem_fb_end_cpu_access(fb, DMA_FROM_DEVICE);

  waveshare_gray8_to_mono(buf, fb->width, fb->height);

  wait_for_redraw(epd, buf);

  /* FIXME: Do i need this copy, if so for what? Is there a damage event some
   *        how?
   */
  memcpy(epd->current_frame, buf, fb->width * fb->height / 8);

out_free:
  kfree(buf);
out_exit:
  drm_dev_exit(idx);

  return ret;
}

static void power_off(struct waveshare_epd *epd) {
  printk("power_off\n");
  printk("FIXME: Try to figure out how to power offthis device\n");
  printk("FIXME: There is no example code for that :/\n");
}

static bool uc8176_display_refresh(struct waveshare_epd *epd) {
  uc8176_write_cmd(epd, UC8176_DISPLAY_REFRESH);
  usleep_range(50000, 100000);

  if (!wait_for_display(epd)) {
    return false;
  }
  return true;
}

static void clear_screen(struct waveshare_epd *epd) {
  u8 *buf;
  buf = kmalloc_array(epd->width/8, epd->height, GFP_KERNEL);
  if (!buf) {
    return;
  }
  memset(buf, 0xFF, epd->height * epd->width / 8);
  uc8176_display_transmission_1(epd, buf);
  uc8176_display_transmission_2(epd, buf);

  uc8176_display_refresh(epd);

  kfree(buf);
}

static void uc8176_reset(struct waveshare_epd *epd) {
  /* Manual says
     When RST_N become low, driver will reset
     this code seems to be a bit whacky
  */
  int i;
  for (i = 0; i < 3; i++) {
    gpiod_set_value_cansleep(epd->reset, 1);
    usleep_range(10000, 20000);
    gpiod_set_value_cansleep(epd->reset, 0);
    usleep_range(1000, 2000);
  }

  gpiod_set_value_cansleep(epd->reset, 1);
  usleep_range(10000, 20000);
}

static void waveshare_pipe_enable(struct drm_simple_display_pipe *pipe,
                                  struct drm_crtc_state *crtc_state,
                                  struct drm_plane_state *plane_state) {
  struct waveshare_epd *epd = drm_to_epd(pipe->crtc.dev);
  struct spi_device *spi = epd->spi;
  struct device *dev = &spi->dev;
  int idx;

  if (!drm_dev_enter(pipe->crtc.dev, &idx))
    return;

  DRM_DEBUG_DRIVER("\n");

  uc8176_reset(epd);

  uc8176_write_cmd(epd, UC8176_POWER_SETTING);
  /* EPD_4IN2_SendData(0x03) */
  uc8176_write_data(epd, 0x03);
  /* EPD_4IN2_SendData(0x00) */
  uc8176_write_data(epd, 0x00);
  /* EPD_4IN2_SendData(0x2b) */
  uc8176_write_data(epd, 0x2b);
  /* EPD_4IN2_SendData(0x2b) */
  uc8176_write_data(epd, 0x2b);
  uc8176_write_cmd(epd, UC8176_BOOSTER_SOFT_START);
  /* EPD_4IN2_SendData(0x17);		//A */
  uc8176_write_data(epd, 0x17);
  /* EPD_4IN2_SendData(0x17);		//B */
  uc8176_write_data(epd, 0x17);
  /* EPD_4IN2_SendData(0x17);		//C */
  uc8176_write_data(epd, 0x17);

  uc8176_write_cmd(epd, UC8176_POWER_ON);

  if (!wait_for_display(epd)) {
    DRM_DEV_ERROR(dev, "timeout waiting for panel to become ready.\n");
    power_off(epd);
    goto out_exit;
  }

  uc8176_write_cmd(epd, UC8176_PANEL_SETTING);
  /* EPD_4IN2_SendData(0xbf); // KW-BF   KWR-AF // BWROTP 0f	BWOTP 1f */
  uc8176_write_data(epd, 0xbf);
  /* EPD_4IN2_SendData(0x0d) */
  uc8176_write_data(epd, 0x0d);

  uc8176_write_cmd(epd, PLL_CONTROL);
  /* EPD_4IN2_SendData(0x3C); // 3A 100HZ   29 // 150Hz 39 200HZ	31 171HZ
   */
  uc8176_write_data(epd, 0x3C);

  uc8176_write_cmd(epd, UC8176_RESOLUTION_SETTING);
  /* EPD_4IN2_SendData(0x01) */
  uc8176_write_data(epd, 0x01); // ;
  /* EPD_4IN2_SendData(0x90); //128 */
  uc8176_write_data(epd, 0x90);
  /* EPD_4IN2_SendData(0x01) */
  uc8176_write_data(epd, 0x01);
  /* EPD_4IN2_SendData(0x2c) */
  uc8176_write_data(epd, 0x2c);

  uc8176_write_cmd(epd, UC8176_VCM_DC_SETTING);
  /* EPD_4IN2_SendData(0x28) */
  uc8176_write_data(epd, 0x28);

  uc8176_write_cmd(epd, UC8176_VOM_AND_DATA_INTERVAL_SETTING);
  /*
   * EPD_4IN2_SendData(0x97); // 97white border
   * 77black border		VBDF 17|D7 VBDW 97 VBDB 57
   * VBDF F7 VBDW 77 VBDB 37  VBDR B7
   */
  uc8176_write_data(epd, 0x97);

  set_lut(epd);

  clear_screen(epd);

out_exit:
  drm_dev_exit(idx);
}

static void waveshare_pipe_disable(struct drm_simple_display_pipe *pipe) {
  printk("waveshare_pipe_disable\n");
  printk("FIXME: Try to figure out how to shut down this device\n");
  printk("FIXME: There is no example code for that :/\n");
}

static void waveshare_pipe_update(struct drm_simple_display_pipe *pipe,
                                  struct drm_plane_state *old_state) {
  struct drm_plane_state *state = pipe->plane.state;
  struct drm_rect rect;

  if (!pipe->crtc.state->active)
    return;

  if (drm_atomic_helper_damage_merged(old_state, state, &rect))
    waveshare_fb_dirty(state->fb);
}

static const struct drm_simple_display_pipe_funcs waveshare_pipe_funcs = {
    .enable = waveshare_pipe_enable,
    .disable = waveshare_pipe_disable,
    .update = waveshare_pipe_update,
};

static int waveshare_connector_get_modes(struct drm_connector *connector) {
  struct waveshare_epd *epd = drm_to_epd(connector->dev);
  struct drm_display_mode *mode;

  mode = drm_mode_duplicate(connector->dev, epd->mode);
  if (!mode) {
    DRM_ERROR("Failed to duplicate mode\n");
    return 0;
  }

  drm_mode_set_name(mode);
  mode->type |= DRM_MODE_TYPE_PREFERRED;
  drm_mode_probed_add(connector, mode);

  connector->display_info.width_mm = mode->width_mm;
  connector->display_info.height_mm = mode->height_mm;

  return 1;
}

static const struct drm_connector_helper_funcs waveshare_connector_hfuncs = {
    .get_modes = waveshare_connector_get_modes,
};

static const struct drm_connector_funcs waveshare_connector_funcs = {
    .reset = drm_atomic_helper_connector_reset,
    .fill_modes = drm_helper_probe_single_connector_modes,
    .destroy = drm_connector_cleanup,
    .atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
    .atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
};

static const struct drm_mode_config_funcs waveshare_mode_config_funcs = {
    .fb_create = drm_gem_fb_create_with_dirty,
    .atomic_check = drm_atomic_helper_check,
    .atomic_commit = drm_atomic_helper_commit,
};

static const uint32_t waveshare_formats[] = {
    DRM_FORMAT_XRGB8888,
};

static const struct of_device_id waveshare_of_match[] = {
    {.compatible = "waveshare,e4200", .data = (void *)E4200},
    {},
};
MODULE_DEVICE_TABLE(of, waveshare_of_match);

static const struct spi_device_id waveshare_id[] = {
    {"e4200", E4200},
    {},
};
MODULE_DEVICE_TABLE(spi, waveshare_id);

DEFINE_DRM_GEM_CMA_FOPS(waveshare_fops);
static const struct drm_driver waveshare_driver = {
    .driver_features = DRIVER_GEM | DRIVER_MODESET | DRIVER_ATOMIC,
    .fops = &waveshare_fops,
    DRM_GEM_CMA_DRIVER_OPS_VMAP,
    .name = "waveshare",
    .desc = "Waveshare e-ink panels",
    .date = "20221224",
    .major = 1,
    .minor = 0,
};

static const struct drm_display_mode waveshare_e4200_mode = {
    DRM_SIMPLE_MODE(400, 300, 84, 63),
};

static int waveshare_probe(struct spi_device *spi) {
  const struct drm_display_mode *mode;
  const struct spi_device_id *spi_id;
  struct device *dev = &spi->dev;
  enum waveshare_model model;
  struct waveshare_epd *epd;
  size_t line_buffer_size;
  struct drm_device *drm;
  const void *match;
  int ret;

  match = device_get_match_data(dev);
  if (match) {
    model = (enum waveshare_model)match;
  } else {
    spi_id = spi_get_device_id(spi);
    model = (enum waveshare_model)spi_id->driver_data;
  }

  /* The SPI device is used to allocate dma memory */
  if (!dev->coherent_dma_mask) {
    ret = dma_coerce_mask_and_coherent(dev, DMA_BIT_MASK(32));
    if (ret) {
      dev_warn(dev, "Failed to set dma mask %d\n", ret);
      return ret;
    }
  }

  epd = devm_drm_dev_alloc(dev, &waveshare_driver, struct waveshare_epd, drm);
  if (IS_ERR(epd))
    return PTR_ERR(epd);

  drm = &epd->drm;

  ret = drmm_mode_config_init(drm);
  if (ret)
    return ret;
  drm->mode_config.funcs = &waveshare_mode_config_funcs;

  epd->spi = spi;

  epd->dc = devm_gpiod_get(dev, "dc", GPIOD_OUT_LOW);
  if (IS_ERR(epd->dc)) {
    ret = PTR_ERR(epd->dc);
    if (ret != -EPROBE_DEFER)
      DRM_DEV_ERROR(dev, "Failed to get gpio 'dc'\n");
    return ret;
  }

  epd->reset = devm_gpiod_get(dev, "reset", GPIOD_OUT_LOW);
  if (IS_ERR(epd->reset)) {
    ret = PTR_ERR(epd->reset);
    if (ret != -EPROBE_DEFER)
      DRM_DEV_ERROR(dev, "Failed to get gpio 'reset'\n");
    return ret;
  }

  epd->busy = devm_gpiod_get(dev, "busy", GPIOD_IN);
  if (IS_ERR(epd->busy)) {
    ret = PTR_ERR(epd->busy);
    if (ret != -EPROBE_DEFER)
      DRM_DEV_ERROR(dev, "Failed to get gpio 'busy'\n");
    return ret;
  }

  switch (model) {
  case E4200:
    mode = &waveshare_e4200_mode;
    epd->stage_time =
        480; // FIXME: calculate time for fast lut 3Hz should be possible.
    epd->bytes_per_scan =
        96 / 4; // FIxME: we need to change that to a more sane default
    break;
  default:
    return -ENODEV;
  }

  epd->mode = mode;
  epd->width = mode->hdisplay;
  epd->height = mode->vdisplay;
  epd->factored_stage_time = epd->stage_time;

  line_buffer_size = 2 * epd->width / 8 + epd->bytes_per_scan + 2;
  epd->line_buffer = devm_kzalloc(dev, line_buffer_size, GFP_KERNEL);
  if (!epd->line_buffer)
    return -ENOMEM;

  epd->current_frame =
      devm_kzalloc(dev, epd->width * epd->height / 8, GFP_KERNEL);
  if (!epd->current_frame)
    return -ENOMEM;

  drm->mode_config.min_width = mode->hdisplay;
  drm->mode_config.max_width = mode->hdisplay;
  drm->mode_config.min_height = mode->vdisplay;
  drm->mode_config.max_height = mode->vdisplay;

  drm_connector_helper_add(&epd->connector, &waveshare_connector_hfuncs);
  ret = drm_connector_init(drm, &epd->connector, &waveshare_connector_funcs,
                           DRM_MODE_CONNECTOR_SPI);
  if (ret)
    return ret;

  ret = drm_simple_display_pipe_init(
      drm, &epd->pipe, &waveshare_pipe_funcs, waveshare_formats,
      ARRAY_SIZE(waveshare_formats), NULL, &epd->connector);
  if (ret)
    return ret;

  drm_mode_config_reset(drm);

  ret = drm_dev_register(drm, 0);
  if (ret)
    return ret;

  spi_set_drvdata(spi, drm);

  DRM_DEBUG_DRIVER("SPI speed: %uMHz\n", spi->max_speed_hz / 1000000);

  drm_fbdev_generic_setup(drm, 0);
  return 0;
}

static int waveshare_remove(struct spi_device *spi) {
  struct drm_device *drm = spi_get_drvdata(spi);

  drm_dev_unplug(drm);
  drm_atomic_helper_shutdown(drm);

  return 0;
}

static void waveshare_shutdown(struct spi_device *spi) {
  drm_atomic_helper_shutdown(spi_get_drvdata(spi));
}

static struct spi_driver waveshare_spi_driver = {
    .driver =
        {
            .name = "waveshare",
            .of_match_table = waveshare_of_match,
        },
    .id_table = waveshare_id,
    .probe = waveshare_probe,
    .remove = waveshare_remove,
    .shutdown = waveshare_shutdown,
};

module_spi_driver(waveshare_spi_driver);

MODULE_AUTHOR("Bernhard Guillon");
MODULE_DESCRIPTION("Waveshare DRM driver");
MODULE_LICENSE("GPL");
