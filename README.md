# UC8176 based E-Ink Linux DRM driver for Waveshare 4.2 grayscale E-Ink display

## Merry xmas

I used the holydays to port my framebuffer driver to DRM :) You can find the old framebuffer driver in git history ;).

## Overview

This is a DRM driver for Waveshare 4.2 4 grayscale E-Ink display. To be used on the raspberry pi zero. And currently in an early state!

It contains the following module:

* waveshare

And the following dts:
* rpi-waveshare-4in2.dts

This work is based on
* repaper.c from raspberry kernel 5.15.76+
* https://github.com/nxthongbk/MangoH-Waveshare (GPL)
* https://github.com/dpfrey/mangOH_Waveshare_Eink (GPL)
* https://github.com/waveshare/e-Paper (MIT like)
* https://benkrasnow.blogspot.com/2017/10/fast-partial-refresh-on-42-e-paper.html (quick lut)

## What works?

Basic DRM driver is working fine, but it has a flashy border. You can use the console and wayland. Refresh rate is about 0.844 FPS.

## Notes

The current code uses [quick lut](https://benkrasnow.blogspot.com/2017/10/fast-partial-refresh-on-42-e-paper.html). This results in fast display refresh rates, but has the drawback of possible burn ins to the display! And there is more tearing. You can clear the image manually with positive and negative full images. I might add a option to the driver to use the slow lut, which are provided by the waveshare examples. But I use it a lot with the quick lut and have no problems so far (tm)

## What is broken?

Currently the lut is work in progress and the device only supports monochrome. Even it could do 4 gray scale. For gray scale to work we need to create a special quick lut for it.

## How can I help?

Every help on this topic is more then welcome. The code is currently not in the shape to submit it to mainline. I'm happy for every pull request to make that situation better.
The display uses a so called [UC8176](https://www.waveshare.com/w/upload/8/88/UC8176.pdf) controller. Every distributor of such controllers rebrand them and change some bits. The lut registers are not provided in that documentation. A similar controller called [IL0373](https://www.smart-prototyping.com/image/data/9_Modules/EinkDisplay/GDEW0154T8/IL0373.pdf) exists with some information about the lut.


## Build the module

On the raspberry you need the latest bullseye release. With that install the kernel headers:

```bash
sudo apt install raspberrypi-kernel-headers build-essential
```

After that a simple

```bash
KERNEL_SRC=/lib/modules/$(uname -r)/build make
```

will do the job.

## Build the overlay

We need to disable the chip select 0 and "glue" our GPIO's with a device tree overlay.

```bash
make dtbo
```

## Load the module

```bash
sudo dtoverlay -d .  rpi-waveshare-4in2.dtbo
sudo insmod waveshare.ko
```

## Get the printk output to the console

```bash
sudo su
echo "8" > /proc/sys/kernel/printk
```

## Test the module

With some randomness.

```bash
cat /dev/urandom > /dev/fb0
```

Or just start a wayland session or use the virtual console.

## Test wayland
If you are brave you can use my [meta-free-fly-zero](https://gitlab.com/mrnice/meta-free-fly-zero) [yoe](https://www.yoedistro.org/) layer to generate a wayland test image.

## TODO
* Fix all FIXMEs from waveshare.c
* Refresh rate
* Port C89 to C11 as the Linux kernel now supports this newer C standard
* The border is flashy, try to disable the border or find some workaround
* Add a description on how to cross compile that driver
* Add help on how to add the overlay and driver at boot
* Add a cross compile step for CI and provide binaries
* Format code according to kernel guidelines
