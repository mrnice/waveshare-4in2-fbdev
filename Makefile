obj-m += waveshare.o
SRC := $(shell pwd)

all:
	make -C $(KERNEL_SRC) M=$(SRC) modules

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules_install

dtbo:
	dtc rpi-waveshare-4in2.dts -O dtb > rpi-waveshare-4in2.dtbo

format:
	clang-format -i *.[ch]

clean:
	make -C $(KERNEL_SRC) M=$(SRC) clean

